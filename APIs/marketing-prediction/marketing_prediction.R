# Marketing Prediction  
#
# Demo prediction API using the public dataset `Portugese marketing campaigns` (see UCI Machine Learning Repo)
#
# Nick Burns
# Aug 2019

library(data.table)
library(bnlearn)
library(jsonlite)
library(plumber)

get_data <- function(filename = './data/bank-additional-full.csv.gz', augment = TRUE) {
  
  if (!file.exists(filename)) {
    print(sprintf("Cannot find the data file: %s", filename))
    
    return (-1)
  }
  
  data <- fread(filename)
  
  if (file.exists('./data/bank-augmented.csv.gz') & augment) {
    data <- rbind(data, fread('./data/bank-augmented.csv.gz'), fill = TRUE)
  }
  return (data[sample(.N)])
}
BANK <- get_data(augment = FALSE)
AUGMENT <- get_data(augment = TRUE)

get_model <- function(filename = './models/marketing_model.Rdata') {
  if (!file.exists(filename)) {
    print(sprintf("Cannot find the model file: %s", filename))
    
    return (-1)
  }
  return (readRDS(filename))
}
save_model <- function(model, filename = './models/marketing_model.Rdata') {
  saveRDS(model, filename)
}
feature_engineering <- function(X, method = 'train')  {
  
  # This is v. frustrating
  # But, I need to load the training data to make sure
  # that the factor levels all align properly.
  # This will slow down the serving of these models... but it's not a deal breaker
  n <- nrow(X)
  #dict <- get_data()
  #datatypes <- dict[, sapply(.SD, is.numeric)]
  datatypes <- AUGMENT[, sapply(.SD, is.numeric)]
  xs <- if (method == 'train') X
        else {
          #rbind(dict, X, fill = TRUE)
          rbind(BANK, X, fill = TRUE)
        }
  
  for (i in 1:ncol(xs)) {
    x <- colnames(xs)[i]
    
    if (!datatypes[i]) {
      xs[is.na(get(x)), (x) := '']
      xs[, (x) := factor(get(x))]
    } else {
      xs[, (x) := as.numeric(get(x))]
      xs[is.na(get(x)), (x) := 0]
      
      if (min(xs[[x]]) > 0) {
        xs[, (x) := log1p(get(x))]
      }
    }
    if (x == 'campaign' | x == 'previous') {
      xs[, (x) := factor(get(x))]
    }
  }
  xs[, y := factor(y)]
  
  
  # drop, as it is an indicator of the outcome
  if ('duration' %in% colnames(xs)) {
    xs[, duration := NULL]
  }
  if ('pdays' %in% colnames(xs)) {
    xs[, pdays := NULL]
  }
  if ('month' %in% colnames(xs)) {
    xs[, month := NULL]
  }
  if ('day_of_week' %in% colnames(xs)) {
    xs[, day_of_week := NULL]
  }
  if ('campaign' %in% colnames(xs)) {
    xs[, campaign := NULL]
  }
  
  results <- if(method == 'train') xs
  else if (n == 1) xs[nrow(xs)]
  else xs[(nrow(xs) - n + 1):nrow(xs)]
  
  return (results)
}
train <- function() {
  xs <- feature_engineering(get_data(augment = TRUE))
  dag <- hc(xs)
  model <- bn.fit(dag, data = xs)
  save_model(model)
}

get_prediction <- function(X, filename = './models/marketing_model.Rdata') {
  model <- get_model(filename)
  test_data <- feature_engineering(copy(X), method = 'predict')
  yhat <- predict(model, node='y', data = test_data, method='bayes-lw')
  
  return (yhat)
}

query <- function(X, k = 100, filename = './models/marketing_model.Rdata') {
  model <- get_model(filename)
  input_columns <- colnames(X)
  test_data <- feature_engineering(copy(X), method = 'predict')
  
  # Nasty for loop to iterate over the inputs...
  results <- matrix(0, nrow = k, ncol = nrow(test_data))
  for (i in 1:nrow(test_data)) {
    
    # This is a nasty for-loop to build up my evidence
    # as a giant string and to pass into cpquery()
    # Note, you can't pass in HEAPS and HEAPS of evidence, otherwise you get very extreme predictions
    # (like predicting the majority class with certainty). So, I have reduced the number of features
    # which are included in the evidence.
    # This is great though, it aligns well with a "causal" or "expert" model
    evi <- list()
    incr <- 0
    for (j in c('age', 'euribor3m', 'loan', 'cons.price.idx')) {
      if (j %in% input_columns) {
        
        value <- test_data[i][[j]]
        tmp <- if(is.numeric(value)) {
          ifelse(value != 0, sprintf("(%s >= %s) & (%s <= %s)", j, value * 0.95, j, value * 1.05), '')
        } else {
          sprintf("(%s == '%s')", j, value)
        }
        
        if (tmp != '') {
          incr <- incr + 1
          evi[[incr]] <- tmp
        }
      }
    }
    evi <- paste(unlist(evi), sep="", collapse = " & ")
    cmd <- sprintf("cpquery(model, (y == 'yes'), %s, method='ls')", evi)
    results[, i] <- replicate(k, eval(parse(text = cmd)))

  }
  
  # collate the results, returning the HPDI
  predictions <- data.table(
    lower = apply(results, 2, function (x) x[order(x)][k * 0.11]),
    yhat = colMeans(results),
    upper = apply(results, 2, function (x) x[order(x)][k * 0.89])
  )
  
  return (predictions)
}


# There is a huge imbalance between the 'yes' and 'no' outcomes of y.
# So, we will use an initial training of the model to augment new values
# for y, which we can then incorporate back into the training routines.
balance_data <- function () {
  
  model <- get_model()
  impute <- function (x, target) {
    predict(model, node=target, data=x, method='bayes-lw')
  } 
  X <- feature_engineering(copy(BANK))
  new_data <- rbindlist(
    lapply(1:10, function (i) {
      print(i)
      tmp <- X[y == 'yes'][sample(.N, 2500, replace = TRUE)]
      xs <- copy(tmp)
      for (j in colnames(xs)) {
        xs[, (j) := impute(xs, j)]
      }
      
      return (xs)
    })
  )
  
  # again, I am going to subsample from the new_data
  # which has a relatively balanced mix of 'yes' and 'no'
  # I want to keep some fo these 'no's, but not all of them
  new_data <- rbind(
    new_data[y == 'yes'],
    new_data[y == 'no'][sample(.N, 8000)]
  )
  write.csv(new_data[sample(.N)], "./data/bank-augmented.csv", quote = FALSE, row.names = FALSE)
  system(sprintf("gzip %s", "./data/bank-augmented.csv"))
}
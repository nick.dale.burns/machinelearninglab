# Data Science Training Resources  

If you've made it to this repo, then you're probably interested in learning you some data science! Buckle up and brace 
yourself :) In all honestly, which ever type of "data scientist" you aspire to be, there is a lot of learning to be done. But 
the good news is, you can get up and running quite quickly (probably within a few weeks) and then you simply continue to 
extend your knowledge as you hit new problems, or get stuck into certain areas. Let's begin by looking at the different types 
of data scientists:

## Flavours of Data Science  

There is a huge range of "data scientists". It is a broad field, which people dump all sorts of different roles into. There 
are heaps of resources on the web, lots of great blogs. But here is my take on what "data science" is.

**Defining Data Science**  

Personally, I like the definition of data science which Sir David Cox proposed: "data science is about maximising the 
*utility* of data". Cox contrasts this with statistics, which he believes is about understanding and quantifying the 
uncertainty in data. Cox gave a talk about this to the Royal Statistical Society in London. I wish I could find it again!   

For me, this is exactly what data science is: the ability to use data to develop business solutions. How do we directly 
measure an outcome of interest? If we can measure it, then can we also predict this outcome early in a business process and 
can we intervene, or change our approach to maximise the outcome that we want to achieve?   

To illustrate this, let's consider one of our use cases here at SQL Services. Traditionally, we have focused on managed database services. We
look after your databases for you, you don't have to worry about it - we've got it. If you don't hear from us, then everything is fine. However, 
this is becoming increasingly difficult to sell. Our customers are expecting more from us. So, our strategy is changing. We need to be increasing the 
level of customer-engagement, we need to spend less time looking after their backups and optimisations, and more time talking to them about how we 
can optimise their performance, streamline their reporting, use the data to shine a light on business performance etc. etc. etc...  

Increasing customer engagement is first and foremost a business problem. But, we can use "data science" to help. We already know what our DBA team 
do all day long, through their service tickets and time entry notes. To align this to our customer engagement strategy, we need to score the time entries. 
The scoring system can be very simple: +1 if an activity promotes a positive customer outcome, 0 if it is neutral and -1 if it is counter-productive. Although 
simple, we can use this scoring system track the daily progress of each and every team member, just like the "worm" that they use in cricket games to track
run rate. At the end of each day, we can review who had a positive day and who had a neagtive day. "Data science" plays a number of roles in this:  

  - initially, we need to manually review a whole lot of activities and manually score these. However, once we have this training set we can develop 
a predictive model to score future activities. Data science has an advantage here in that it can handle the unstructured text of DBA time entries without having
to manually develop a potentially complex rule set. We also don't need to be overly prescriptive about the content in these time entry notes. Machine Learning 
models have got very good at handling language models, typos, slight variations of a theme etc.  
  - once we can score a day, then we can develop a descriptive model to classify "positive" days from "negative" days. What activities reliably result in positive outcomes? 
How do we promote these activities? What activities reliably result in negative outcomes? How do we remove these from the workday?  
  - finally, if we can score a day, then we should be able to predict the outcome early in the working day. If we can reliable say, say at 11AM, that a workday is heading in 
a negative direction, then we can intervene and turn this around. Why is this day off-track? Has this DBA been stuck on low-return tasks? If we were to put 2 additional 
resources on this for 40 min, can we clear these tasks and turn everyone's day around?  

When Sir David Cox talks about the "utility of data", I believe that he is talking about these types of scenarios. We don't have to be world-class 
mathematicians to do this type of work. The skills that go into developing these systems are not a big reach and are quite attainable. So what "types of data scientists" 
are there?  

**Citizen Data Scientists**  
This is an increasingly popular term, usually promoted by vendors such as Tableau, Data Robot, Microsoft (PowerBI) and so on. These people might be business analysts, sales people,
or managers. Basically anyone who is interested in using data to drive their decisions. These people are probably all "data literate", comfortable with visualising data, 
interpretting basic data summaries (averages, min / max, trends) and happy to explore their data for insights. These people may do simple data transformations and 
"prepared" analyses (e.g. regression in Excel, pivot tables, sum / mean / max etc. in excel or PowerBI). These people are likely to rely on clean, prepared datasets.   

**BI developers and engineers**  
I don't believe you can have a discussion on data science without discussing BI. Data science requires data and a huge part of working with data involves data cleaning, ETL 
and standardisation of data (i.e. master data management). I am also going to put "big data engineers" in this group. This group is fundamentally about data pipelining 
and data transformation.  

**Business sponsors**  
Similar to BI, data science cannot succeed without business sponsorship. "Utilising" data is fundamentally a business problem: what process do we want to optimise? 
What would a good outcome be, what would a bad outcome be? If we could develop the "perfect" solution, how would we use this? How would we plug this into daily processes
and use the outputs to improve our business outcome? Ultimately, I believe that business stakeholders are fundamentally responsible for driving data science efforts and success. 
I am very skeptical of anyone that thinks you can "sprinkle a bit of machine learning" around and achieve great things.  

**Machine Learning Engineers**  
I believe that this is what a lot of people think of when they think of data science: taking data, developing predictive models and deploying these models into production. 
Machine learning engineers can all code. They are probably developers, or computer science graduates, or perhaps people who have self-trained in something like R or python. 
I would expect a machine learning engineer to be able to take a dataset and an outcome and develop the "best" predictive model for the scenario. Machine learning engineers 
will have strong familiarity with machine learning frameworks, such as sklearn, keras, Microsoft's Cognitive Services etc. I would expect a machine learning engineer to be able to deploy a model and 
help end users understand why the machine came up with a particular prediction.  

**Developers**  
Just like we need data engineers, we need developers to close the loop. Developers might also be machine learning engineers, but they should also be great web/app developers, familiar 
with APIs and how to inject machine learning outputs into an app / business process. The best results happen when machine learning outputs can be injected directly into the 
day-to-day operations.  

**Data scientists**  
Finally, we have the "statistician" (I use that term loosely). This person should have advanced skills in mathematics, statistics and machine learning / deep learning. They should
be able to understand how to use data to answer questions, be intimately familiar with sources of bias and uncertainty and how to translate business problems into data problems. 
The data scientist is likely to be responsible for defining the direction of a data science project and the methods which are appropriate to solve the problem. These people can all 
code in something like R or Python to manipulate data, explore and visualise data and to develop techniques. They should have sufficient technical & theoretical skill to implement 
solutions outside of standard frameworks and to solve challenging problems.  

## Online Training Resources  

There are LOADS of online training resources. The best ones are all available for free (unless you want the certificate). Personally, I am a fan of the platforms Coursera, EdX and Udacity. 
These platforms are developed and curated by the best universities in the world and companies like Microsoft, Google, Facebook and AT&T. Bottom line, you can be sure of the quality of 
the content. In contrast, there isn't the same level of curation (or deep understanding) on platforms like Udemy or Pluralsight (my opinion).  

So you're interested in data science? I would begin in one of the following places:  

**Microsoft's Data Science Programme on EdX**  
This is a series of courses which begin with basic visualisation and Excel, move onto PowerBI, SQL, R, Python and Machine Learning. Each course is designed to take about a month (~20 hours 
of effort). 
This whole program is very practical, and obviously uses Microsoft tools, which means you'll be up and running in a short time. On the downside (perhaps), it is also very broad and tends 
to skim over the surface of the topics. This is very much a "breadth-first" approach to starting in data science. I like it.  

URL: https://www.edx.org/microsoft-professional-program-data-science  

**John Hopkins University Data Science Specialisation on Coursera**  
Like Microsoft's course, the John Hopkins course is a series of short courses covering data analysis, visualisation, statistics, machine learning and statistical inference (interpretting your models). 
The John Hopkins course focuses on the use of R to analyse data. It is taught by some of the best computational statisticians in the world and is a brilliant intro to applied statistics. This 
course is more theoretical than the Microsoft course and will give you a better grounding in the data analysis process.  

URL: https://www.coursera.org/specializations/jhu-data-science  

**Andrew Ng's Machine Learning Course on Coursera**  
This course is the canonical course in machine learning, lead by Andrew Ng who has previously been the chief data scientist at Baidu (China's Google), is a professor at Stanford and a 
very 
successful data science consultant in the Bay Area (San Francisco). This course is taught using Octave (an open-source spinoff of Matlab) and Ng staunchly defends this, as this course is about 
understanding the fundamentals of machine learning, not just how to call a python library. That said, you will find GitHub repos with python implementations too. If you want to be a machine learning 
engineer or data scientist, you should definitely do this course.  

URL: https://www.coursera.org/learn/machine-learning  

**Udacity's Machine Learning NanoDegree**  
Finally, Udacity have made a real name for themselve offering high-quality practical nanodegrees. These courses are a collaboration between Google, Facebook, AT&T and leading universities (e.g. 
University of Berkley). They are a great mid-point between the practical intro of Microsoft's courses and Ng's robust course. Unfortunately, there is a cost associated with some of thsee courses, but 
they are quite affordable (monthly subscription). In the past, I have done Udacity's computer science and python courses and they were sensational. 

URL: https://www.udacity.com/course/machine-learning-engineer-nanodegree--nd009t  


